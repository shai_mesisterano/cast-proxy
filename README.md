# CAST Proxy

> Simulates CAST ads in websites

## Installation

1. Install Node.js on your machine (download from: [here](http://nodejs.org/ "http://nodejs.org/")).

2. In command line, go to the project root, (i.e. `C:\work\cast-proxy`).

3. Run `npm install` from the project directory.

4. Run `node server` from the project root to start a local web server.

5. Run `forever start server.js` from the project root to run server continuously.

## Update

1. Push to git branch.

2. In command line, go to the project root, (i.e. `C:\work\cast-proxy`).

3. Run `git pull`.

4. Run `forever restartall`.

## Examples

1. http://p.afterdl.com/gladinga-flyfishing.blogspot.co.il/tabs-outer/2/289/Web

2. Interstitial (Mobile Web): http://p.afterdl.com/mp3jam.org/shadow/3/1515/MobileWeb

## Usage

Just type a URL:

http://[host]/[website]/[HTMLselector]/[networkID]/[adID]/[platform]

And you`ll see:
Your *ad* ("289", "1506"...),
By platform *platform* ("Web", "MobileWeb")
From a *network*, ("1", "2"...)
Inside *HTML Selector* ("container", "footer"...)
As part of the *Website* ("cnn.com", "cnet.com"...)