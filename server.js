var express = require('express'),
    path = require('path'),
    jsdom = require("jsdom");

var app = express();

app.get('/:url/:selector/:network/:ad/:platform', function (req, res) {
    jsdom.env(
        "http://" + req.params["url"],
        function (errors, oldWindow) {
            var newWindow,
                selector,
                adElement,
                adnl_zone,
                base,
                headFirstChild,
                scriptEl,
                castUrl,
                cdnUrl,
                platform;

            // Locate ad element
            selector = req.params["selector"];
            adElement = oldWindow.document.getElementById(selector) ||
                oldWindow.document.getElementsByClassName(selector)[0] ||
                oldWindow.document.getElementsByTagName("body")[0] ||
            {};

            // Add zone DIV to the old window
            adnl_zone = oldWindow.document.createElement('div');
            adnl_zone.className = "adnl_zone id_" + req.params["ad"];
            adElement.appendChild(adnl_zone);

            // Add Script to the old window
            castUrl = '//castqa.blob.core.windows.net';
            cdnUrl = '//cdn.qa.castplatform.com';
            platform = req.params["platform"] || "Web";
            scriptEl = oldWindow.document.createElement('script');
            scriptEl.innerHTML = 'var ADNL = []; \
            ADNL.push(["setConstant", "SCAN_ZONE_CLASS", "adnl_zone"]); \
            ADNL.push(["setConstant", "CDN_URL", "' + cdnUrl + '"]); \
            ADNL.push(["setConstant", "PLATFORM", "' + platform + '"]); \
            ADNL.push(["scanZones"]); \
            ADNL.push(["initialize", "' + req.params["network"] + '"]); \
            (function (d) { \
                var s = d.createElement("script"); \
                s.src = "' + castUrl + '/scripts/' + req.params["network"] + '/adnl.min.js"; \
                s.type = "text/javascript"; \
                d.getElementsByTagName("head")[0].appendChild(s); \
            })(document);';
            adElement.appendChild(scriptEl);

            // Create new window
            newWindow = jsdom.jsdom(oldWindow.document.documentElement.innerHTML, null, {
                features: {
                    FetchExternalResources: ['script'],
                    ProcessExternalResources: false
                }
            }).parentWindow;

            // Add base href to the new window
            base = newWindow.document.createElement("base");
            base.href = "http://" + req.params["url"];
            headFirstChild = newWindow.document.getElementsByTagName("head")[0].firstChild;
            newWindow.document.getElementsByTagName("head")[0].insertBefore(base, headFirstChild);

            // Send response
            res.send(newWindow.document.innerHTML);

            // Free memory
            oldWindow.close();
        }
    );
});

app.use('/', express.static(path.join(__dirname, "public")));
app.listen(80);

console.log("Running...");